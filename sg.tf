# Configure the AWS Provider
provider "aws" {
  region = "var.region"
}

terraform {
  backend "s3" {
    bucket = "demobucket132"
    key    = "newtfstate/vpc"
    region = "var.region"
  }
}
resource "aws_vpc" "main" {
  cidr_block = "var.cidr_block"
}
