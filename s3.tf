provider "aws" {
  region     = "us-east-1"

}
terraform {
  backend "s3" {
    bucket = "demobucket132"
    key    = "terraformstate/vpc/.tfstatefile"
    region = "us-east-1"
  }
}
resource "aws_vpc" "main" {
  cidr_block       = "172.35.1.0/24"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}